<%@page import="fr.eni.javaee.qcm.bo.Utilisateur"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/styles.css">
<title>Insert title here</title>
</head>
<body>
<% String role = (String)session.getAttribute("role"); %>
<c:if test="${not empty utilisateur }">

<nav class="navbar height"  style="background-color: #4c627b;">
    <a class="ml-3" style="color:white; font-size: 1.2rem;"><img src="<%=request.getContextPath()%>/images/user.png" width="30" height="30" class="d-inline-block align-top mr-2" alt="userLogo">
    <span class="mr-3" style="color:#ffc107"><%=role %></span>  ${utilisateur.prenom } ${utilisateur.nom }
    </a>
    <a href="<%=request.getContextPath()%>/candidatAccueil" class="ml-auto mr-5" style="color:white; text-decoration:none;">Accueil</a>
    <a href="<%=request.getContextPath()%>/candidatResultats" class="mr-5" style="color:white; text-decoration:none;">Résultats</a>
    <form method="get" action="<%=request.getContextPath()%>/ServletAuthentification">
    <button type="submit" class="btn btn-outline-warning mr-4">Déconnexion<img src="<%=request.getContextPath()%>/images/airplane-takeoff.png" class="ml-2" alt="logout"></button>
    </form>
</nav>

<div class="container col-10">
<div class="row margin">

</div>
</div>

</c:if>

<div style="height:11.55rem;"></div>
<footer>
	<h6 id="footer">&copy; NABO SUECO</h6>
</footer>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>