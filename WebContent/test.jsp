<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
bonsoir
<% String role = (String)session.getAttribute("role"); %>
<c:if test="${not empty utilisateur }">

<nav class="navbar navbar-light height"  style="background-color: #e3f2fd;">
    <a><img src="<%=request.getContextPath()%>/images/user.png" width="30" height="30" class="d-inline-block align-top mr-2" alt="userLogo">
    <%=role %> : ${utilisateur.prenom } ${utilisateur.nom }
    </a>
    <form method="get" action="">
    <input type="submit" class="ml-auto" value="Déconnexion"> <img src="<%=request.getContextPath()%>/images/airplane-takeoff.png" class="ml-2" alt="logout">
    </form>
</nav>

<div class="container col-10">
<div class="row margin">
	<div class="mx-auto col-10 col-md-6 col-lg-4 categorie">
	<a href="#" style="text-decoration: none; color: black;">
		<img src="<%=request.getContextPath()%>/images/epreuves.jpg" width="100" height="100" class="mx-auto d-block">
		<h3 class="text-center my-3">Epreuves</h3>
		<p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed hendrerit, massa a lobortis malesuada,
		arcu velit condimentum libero, ac sagittis mi sapien at sapien. Nam nec aliquam ante.</p>
	</a>
	</div>
	
	<div class="mx-auto col-10 col-md-6 col-lg-4 categorie">
	<a href="#" style="text-decoration: none; color: black">
		<img src="<%=request.getContextPath()%>/images/resultats.jpg" width="100" height="100" class="mx-auto d-block">
		<h3 class="text-center my-3">Résultats</h3>
		<p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed hendrerit, massa a lobortis malesuada,
		arcu velit condimentum libero, ac sagittis mi sapien at sapien. Nam nec aliquam ante.</p>
	</a>
	</div>

</div>
</div>

</c:if>
</body>
</html>