/**
 * 
 */
package fr.eni.javaee.qcm.bll;

/**
 * @author abidet2017
 * @date 28 mars 2018
 * @version V1.0
 */
/**
 * Les codes disponibles sont entre 20000 et 29999
 */
public abstract class CodeResultatBLL {

	/**
	 * Echec quand la date de début et la date de fin de l'épreuve ne respecte pas les règles définies
	 */
	public static final int REGLE_EPREUVE_DATE_ERREUR=20000;
	
	/**
	 * Echec quand la liste de questions tirées ne respecte pas les règles définies
	 */
	public static final int REGLE_EPREUVE_QUESTION_TIRAGE_ERREUR=20001;
	
	/**
	 * Echec quand la liste de réponses tirées ne respecte pas les règles définies
	 */
	public static final int REGLE_EPREUVE_REPONSE_TIRAGE_ERREUR=20002;
}
