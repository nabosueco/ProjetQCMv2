package fr.eni.javaee.qcm.bll;

import java.time.LocalDate;
import java.util.List;
import java.util.Random;

import fr.eni.javaee.qcm.BusinessException;
import fr.eni.javaee.qcm.bo.Epreuve;
import fr.eni.javaee.qcm.bo.Question;
import fr.eni.javaee.qcm.bo.QuestionTirage;
import fr.eni.javaee.qcm.bo.ReponseTirage;
import fr.eni.javaee.qcm.dal.CodesResultatDAL;
import fr.eni.javaee.qcm.dal.DAOFactory;
import fr.eni.javaee.qcm.dal.EpreuveDAO;

/**
 * @author abidet2017
 * @date 28 mars 2018
 * @version V1.0
 */
public class EpreuveManager {
	private EpreuveDAO epreuveDAO;
	public String error;
	
	public EpreuveManager() {
		this.epreuveDAO=DAOFactory.getEpreuveDAO();
	}
	
	public void ajouterEpreuve(Epreuve e) throws BusinessException {
	
		if(validerEpreuve(e)== false) {
			System.out.println(this.error);
			this.error="erreur";
		} else {
			epreuveDAO.insertEpreuve(e);
		}
	}
	
	
	
	private boolean validerEpreuve(Epreuve e) {
		if(e == null || e.getDebutValidite().isBefore(LocalDate.now()) || e.getDebutValidite().isEqual(LocalDate.now()) || e.getFinValidite().isEqual(LocalDate.now()) || e.getFinValidite().isBefore(LocalDate.now())) {
			return false;
		} else {
			return true;
		}
	}
/*
	private boolean validerQuestionT(QuestionTirage qt) {
		if(qt.getIdQuestion() == 0 || qt.getIdEpreuve() == 0 || qt.getNumOrdre() == 0) {
			this.error="La question doit exister, ou correspondre à une épreuve ou doit contenir un numéro d'ordre";
			return false;
		} else {
			return true;
		}
	}
	
	private boolean validerReponseT(ReponseTirage rt) {
		if(rt.getIdProposition() == 0 || rt.getIdQuestion() == 0 && rt.getIdEpreuve() == 0) {
			this.error="La réponse doit répondre à une proposition, doit correspondre à une question et doit correspondre à une épreuve";
			return false;
		} else {
			return true;
		}
	}
*/
	
	public List<Epreuve> selectionnerToutesLesEpreuves() throws BusinessException{
		return this.epreuveDAO.selectEpreuve();
	}
	
	public List<Epreuve> selectionnerEpreuveDebut(LocalDate Debut) throws BusinessException {
		return this.epreuveDAO.selectEpreuveDebut(Debut);
	}
	
	public List<Epreuve> selectionnerEpreuveFin(LocalDate Fin) throws BusinessException {
		return this.epreuveDAO.selectEpreuveFin(Fin);
	}
	
	public List<Epreuve> selectionnerEpreuveEtat(int etatEpreuve) throws BusinessException {
		return this.epreuveDAO.selectEpreuve(etatEpreuve);
	}
}
