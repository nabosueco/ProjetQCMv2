package fr.eni.javaee.qcm.bll;

import fr.eni.javaee.qcm.bo.Utilisateur;
import fr.eni.javaee.qcm.dal.DAOFactory;
import fr.eni.javaee.qcm.dal.UtilisateurDAO;

public class UtilisateurManager {

	private static UtilisateurDAO utilisateurDAO;
	
	public UtilisateurManager() {
		this.utilisateurDAO = DAOFactory.getUtilisateurDAO();	
	}
	
	public Utilisateur authentifier(String email, String password) {
		Utilisateur user = null;
		try {
			user = utilisateurDAO.selectBy(email, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
		
	}
}
