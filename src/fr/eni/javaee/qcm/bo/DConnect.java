package fr.eni.javaee.qcm.bo;

import java.io.Serializable;

public class DConnect implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String identifiant;
	private String motDePasse;
	private int role;
	private Login login;
	
	public DConnect() {
		this.login = new Login();
	}
	
	private void valider() {
		if(login.authentifier(identifiant, motDePasse)>0) {
			this.role=login.getRole();
		}else {
			afficherErreur();
		}
	}
	private void afficherErreur() {
		int erreur = login.getStatus();
	}
}
