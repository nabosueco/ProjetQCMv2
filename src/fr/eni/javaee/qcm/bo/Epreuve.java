package fr.eni.javaee.qcm.bo;


import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

public class Epreuve implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private int idEpreuve;
	private LocalDate debutValidite;
	private LocalDate finValidite;
	private int tempsEcoule;
	private int etatEpreuve;
	private float noteObtenue;
	private String niveauObtenu;
	private int idTest;
	private int idUtilisateur;
	
	private List<QuestionTirage> listeQuestionT;
	private List<ReponseTirage> listeReponseT;
	
	public Epreuve() {
		super();
	}

	public Epreuve(LocalDate debutValidite, LocalDate finValidite, int tempsEcoule, int etatEpreuve, float noteObtenue,
			String niveauObtenu, int idTest, int idUtilisateur, List<QuestionTirage> listeQuestionT,
			List<ReponseTirage> listeReponseT) {
		super();
		this.debutValidite = debutValidite;
		this.finValidite = finValidite;
		this.tempsEcoule = tempsEcoule;
		this.etatEpreuve = etatEpreuve;
		this.noteObtenue = noteObtenue;
		this.niveauObtenu = niveauObtenu;
		this.idTest = idTest;
		this.idUtilisateur = idUtilisateur;
		this.listeQuestionT = listeQuestionT;
		this.listeReponseT = listeReponseT;
	}

	public Epreuve(int idEpreuve, LocalDate debutValidite, LocalDate finValidite, int tempsEcoule, int etatEpreuve,
			float noteObtenue, String niveauObtenu, int idTest, int idUtilisateur, List<QuestionTirage> listeQuestionT, List<ReponseTirage> listeReponseT) {
		
		this(debutValidite, finValidite, tempsEcoule, etatEpreuve, noteObtenue, niveauObtenu, idTest, idUtilisateur, listeQuestionT, listeReponseT);
		this.idEpreuve = idEpreuve;
	}

	public void insertQuestionT(QuestionTirage r) {
		this.listeQuestionT.add(r);
	}

	public LocalDate getFinValidite() {
		return finValidite;
	}

	public void setFinValidite(LocalDate finValidite) {
		this.finValidite = finValidite;
	}

	public int getEtatEpreuve() {
		return etatEpreuve;
	}

	public void setEtatEpreuve(int etatEpreuve) {
		this.etatEpreuve = etatEpreuve;
	}

	public float getNoteObtenue() {
		return noteObtenue;
	}

	public void setNoteObtenue(float noteObtenue) {
		this.noteObtenue = noteObtenue;
	}

	public String getNiveauObtenu() {
		return niveauObtenu;
	}

	public void setNiveauObtenu(String niveauObtenu) {
		this.niveauObtenu = niveauObtenu;
	}

	public List<QuestionTirage> getListeQuestionT() {
		return listeQuestionT;
	}

	public void setListeQuestionT(List<QuestionTirage> listeQuestionT) {
		this.listeQuestionT = listeQuestionT;
	}
	
	public List<ReponseTirage> getListeReponseT() {
		return listeReponseT;
	}

	public void setListeReponseT(List<ReponseTirage> listeReponseT) {
		this.listeReponseT = listeReponseT;
	}

	public int getIdEpreuve() {
		return idEpreuve;
	}
	
	public void setIdEpreuve(int idEpreuve) {
		this.idEpreuve = idEpreuve;
	}

	public LocalDate getDebutValidite() {
		return debutValidite;
	}

	public void setDebutValidite(LocalDate debutValidite) {
		this.debutValidite = debutValidite;
	}

	public int getTempsEcoule() {
		return tempsEcoule;
	}

	public void setTempsEcoule(int tempsEcoule) {
		this.tempsEcoule = tempsEcoule;
	}

	public int getIdTest() {
		return idTest;
	}

	public void setIdTest(int idTest) {
		this.idTest = idTest;
	}

	public int getIdUtilisateur() {
		return idUtilisateur;
	}

	public void setIdUtilisateur(int idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}

	@Override
	public String toString() {
		return "Epreuve [idEpreuve=" + idEpreuve + ", DebutValidite=" + debutValidite + ", FinValidite=" + finValidite
				+ ", tempsEcoule=" + tempsEcoule + ", etatEpreuve=" + etatEpreuve + ", noteObtenue=" + noteObtenue
				+ ", niveauObtenu=" + niveauObtenu + ", idTest=" + idTest + ", idUtilisateur=" + idUtilisateur
				+ ", listeQuestionT=" + listeQuestionT + ", listeReponseT=" + listeReponseT + "]";
	}
	
}
