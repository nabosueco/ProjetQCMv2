package fr.eni.javaee.qcm.bo;

import java.util.List;

public abstract class Proposition {
	private int idProposition;
	private String enonce;
	private boolean estBonne;
	private List<Integer> idQuestions;
}
