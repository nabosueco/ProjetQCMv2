package fr.eni.javaee.qcm.bo;

import java.io.Serializable;
import java.util.List;

public class QuestionTirage implements Serializable{
	private static final long serialVersionUID = 1L;
	
	
	private int marquee;
	private int idQuestion;
	private int numOrdre;
	private int idQuestion;
	private int idEpreuve;
	private List<ReponseTirage> listeReponseT;
	private Question question;
	
	
	public int getMarquee() {
		return marquee;
	}
	public void setMarquee(boolean marquee) {
		this.marquee = marquee;
	}
	
	public int getIdQuestion() {
		return idQuestion;
	}
	public void setIdQuestion(int idQuestion) {
		this.idQuestion = idQuestion;
	}
	public int getNumOrdre() {
		return numOrdre;
	}
	public void setNumOrdre(int numOrdre) {
		this.numOrdre = numOrdre;
	}
	public int getIdEpreuve() {
		return idEpreuve;
	}
	public void setIdEpreuve(int idEpreuve) {
		this.idEpreuve = idEpreuve;
	}
	
	public List<ReponseTirage> getListeReponseT() {
		return listeReponseT;
	}
	public void setListeReponseT(List<ReponseTirage> listeReponseT) {
		this.listeReponseT = listeReponseT;
	}
	
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
	public QuestionTirage(int marquee, int idQuestion, int numOrdre, int idEpreuve) {
		super();
		this.marquee = marquee;
		this.idQuestion = idQuestion;
		this.numOrdre = numOrdre;
		this.idEpreuve = idEpreuve;
	}

	public QuestionTirage(int marquee, int idQuestion, int numOrdre, int idEpreuve, List<ReponseTirage> listeReponseT,
			Question question) {
		this(marquee, idQuestion, numOrdre, idEpreuve);
		this.listeReponseT = listeReponseT;
		this.question = question;
	}
	
	public QuestionTirage() {
		super();
	}
	
	@Override
	public String toString() {
		return "QuestionTirage [marquee=" + marquee + ", idQuestion=" + idQuestion + ", numOrdre=" + numOrdre
				+ ", idEpreuve=" + idEpreuve + ", listeReponseT=" + listeReponseT + ", question=" + question + "]";
	}
	

	
}
