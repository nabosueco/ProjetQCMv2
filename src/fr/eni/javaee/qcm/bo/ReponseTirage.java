package fr.eni.javaee.qcm.bo;

/**
 * @author abidet2017
 * @date 27 mars 2018
 * @version V1.0
 */
public class ReponseTirage implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private int idProposition;
	private int idQuestion;
	private int idEpreuve;
	private Proposition propostion;
	
	
	public int getIdProposition() {
		return idProposition;
	}

	public void setIdProposition(int idProposition) {
		this.idProposition = idProposition;
	}

	public int getIdQuestion() {
		return idQuestion;
	}

	public void setIdQuestion(int idQuestion) {
		this.idQuestion = idQuestion;
	}

	public int getIdEpreuve() {
		return idEpreuve;
	}
	
	public void setIdEpreuve(int idEpreuve) {
		this.idEpreuve = idEpreuve;
	}
	
	public Proposition getPropostion() {
		return propostion;
	}

	public void setPropostion(Proposition propostion) {
		this.propostion = propostion;
	}
	
	public ReponseTirage() {
		super();
	}
	
	public ReponseTirage(int idProposition, int idQuestion, int idEpreuve) {
		super();
		this.idProposition = idProposition;
		this.idQuestion = idQuestion;
		this.idEpreuve = idEpreuve;
	}
	
	public ReponseTirage(int idProposition, int idQuestion, int idEpreuve, Proposition propostion) {
		super();
		this.idProposition = idProposition;
		this.idQuestion = idQuestion;
		this.idEpreuve = idEpreuve;
		this.propostion = propostion;
	}

	@Override
	public String toString() {
		return "ReponseTirage [idProposition=" + idProposition + ", idQuestion=" + idQuestion + ", idEpreuve="
				+ idEpreuve + ", propostion=" + propostion + "]";
	}



	
	
	
}
