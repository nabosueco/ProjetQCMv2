package fr.eni.javaee.qcm.bo;

import java.io.Serializable;

public class Section  implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String nom;
	private int nbQuestions;
	private int idTest;
	private int idTheme;
	
	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public int getNbQuestions() {
		return nbQuestions;
	}
	
	public void setNbQuestions(int nbQuestions) {
		this.nbQuestions = nbQuestions;
	}
	
	public int getIdTest() {
		return idTest;
	}
	
	public void setIdTest(int idTest) {
		this.idTest = idTest;
	}
	
	public int getIdTheme() {
		return idTheme;
	}
	
	public void setIdTheme(int idTheme) {
		this.idTheme = idTheme;
	}
	
	public Section() {
		super();
	}

	public Section(String nom, int nbQuestions, int idTest, int idTheme) {
		super();
		this.nom = nom;
		this.nbQuestions = nbQuestions;
		this.idTest = idTest;
		this.idTheme = idTheme;
	}

	@Override
	public String toString() {
		return "Section [nom=" + nom + ", nbQuestions=" + nbQuestions + ", idTest=" + idTest + ", idTheme=" + idTheme
				+ "]";
	}
	
	
	
}
