package fr.eni.javaee.qcm.dal;

/**
 * @author abidet2017
 *
 */
public abstract class CodesResultatDAL {
	
	/**
	 * Echec général quand tentative d'ajout d'un utilisateur null
	 */
	public static final int INSERT_UTILISATEUR_NULL=10000;
	
	/**
	 * Echec général quand erreur non gérée à l'insertion d'un utilisateur
	 */
	public static final int INSERT_UTILISATEUR_ECHEC=10001;
	
	/**
	 * Echec de la lecture des utilisateurs
	 */
	public static final int LECTURE_UTILISATEUR_ECHEC=10002;
	
	/**
	 * Echec général quand tentative d'ajout d'une epreuve null
	 */
	public static final int INSERT_EPREUVE_NULL=10003;
	
	/**
	 * Echec général quand erreur non gérée à l'insertion d'un utilisateur
	 */
	public static final int INSERT_EPREUVE_ECHEC=10004;
	
	/**
	 * Echec de la lecture des epreuves
	 */
	public static final int LECTURE_EPREUVE_ECHEC=10005;
}
