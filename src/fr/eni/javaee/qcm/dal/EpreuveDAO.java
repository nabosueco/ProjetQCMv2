package fr.eni.javaee.qcm.dal;


import java.time.LocalDate;
import java.util.List;

import fr.eni.javaee.qcm.BusinessException;
import fr.eni.javaee.qcm.bo.Epreuve;





/**
 * @author abidet2017
 *
 */
public interface EpreuveDAO {

	public void insertEpreuve(Epreuve epreuve) throws BusinessException;
	public List<Epreuve> selectEpreuve() throws BusinessException;
	public List<Epreuve> selectEpreuveDebut(LocalDate Debut) throws BusinessException;
	public List<Epreuve> selectEpreuveFin(LocalDate Fin) throws BusinessException;
	public List<Epreuve> selectEpreuve(int etatEpreuve) throws BusinessException;

	
}
