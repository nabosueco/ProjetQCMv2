package fr.eni.javaee.qcm.dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


import fr.eni.javaee.qcm.BusinessException;
import fr.eni.javaee.qcm.bo.Epreuve;
import fr.eni.javaee.qcm.bo.Question;
import fr.eni.javaee.qcm.bo.QuestionTirage;
import fr.eni.javaee.qcm.bo.ReponseTirage;
import fr.eni.javaee.qcm.dal.CodesResultatDAL;
import jersey.repackaged.com.google.common.util.concurrent.ExecutionError;

/**
 * @author abidet2017
 * @date 27 mars 2018
 * @version V1.0
 */
public class EpreuveDAOJdbcImpl implements EpreuveDAO {

	private static final String INSERT_EPREUVE="insert into epreuve(dateDebutValidite, dateFinValidite, tempsEcoule, etat, note_obtenue, niveau_obtenu, idTest, idUtilisateur) values(?,?,?,?,?,?,?,?)";
	private static final String INSERT_QUESTION_TIRAGE="insert into question_tirage(estMarquee, idQuestion, numordre, idEpreuve) values (?,?,?)";
	private static final String INSERT_REPONSE_TIRAGE="insert into reponse_tirage(idQuestion, idEpreuve) values(?,?)";
	private static final String SELECT_ALL_EPREUVE = " SELECT " +
													 " e.id as idEpreuve," +
													 " e.dateDebutValidite," +
													 " e.dateFinValidite," +
													 " e.tempsEcoule," +
													 " e.etat," +
													 " e.note_obtenue" +
													 " e.niveau_obtenu," +
													 " e.idTest," +
													 " e.idUtilisateur," +
													 " qt.estMarquee," +
													 " qt.id as idQuestion," +
													 " qt.numordre," +
													 " qt.idEpreuve," +
													 " rt.id as idProposition," +
													 " rt.idQuestion," +
													 " rt.idEpreuve" +
													 " FROM" +
													 " EPREUVE E" +
													 " INNER JOIN QUESTION_TIRAGE qt ON e.id=qt.idEpreuve" +
													 " INNER JOIN REPONSE_TIRAGE rt ON qt.id=rt.idQuestion" +
													 " WHERE e.dateDebutValidite=?" +
													 " ORDER BY e.dateDebutValidite desc";
	
	private static final String SELECT_DATE_DEBUT_EPREUVE = " SELECT " +
													 " e.id as idEpreuve," +
													 " e.dateDebutValidite," +
													 " e.dateFinValidite," +
													 " e.tempsEcoule," +
													 " e.etat," +
													 " e.note_obtenue" +
													 " e.niveau_obtenu," +
													 " e.idTest," +
													 " e.idUtilisateur," +
													 " qt.estMarquee," +
													 " qt.id as idQuestion," +
													 " qt.numordre," +
													 " qt.idEpreuve," +
													 " rt.id as idProposition," +
													 " rt.idQuestion," +
													 " rt.idEpreuve" +
													 " FROM" +
													 " EPREUVE E" +
													 " INNER JOIN QUESTION_TIRAGE qt ON e.id=qt.idEpreuve" +
													 " INNER JOIN REPONSE_TIRAGE rt ON qt.id=rt.idQuestion" +
													 " WHERE e.dateDebutValidite=?" +
													 " ORDER BY e.dateDebutValidite desc";
	
	private static final String SELECT_DATE_FIN_EPREUVE = " SELECT " +
													 " e.id as idEpreuve," +
													 " e.dateDebutValidite," +
													 " e.dateFinValidite," +
													 " e.tempsEcoule," +
													 " e.etat," +
													 " e.note_obtenue" +
													 " e.niveau_obtenu," +
													 " e.idTest," +
													 " e.idUtilisateur," +
													 " qt.estMarquee," +
													 " qt.id as idQuestion," +
													 " qt.numordre," +
													 " qt.idEpreuve," +
													 " rt.id as idProposition," +
													 " rt.idQuestion," +
													 " rt.idEpreuve" +
													 " FROM" +
													 " EPREUVE E" +
													 " INNER JOIN QUESTION_TIRAGE qt ON e.id=qt.idEpreuve" +
													 " INNER JOIN REPONSE_TIRAGE rt ON qt.id=rt.idQuestion" +
													 " WHERE e.dateFinValidite=?" +
													 " ORDER BY e.dateFinValidite desc";
	
	public static final String SELECT_ETAT_EPREUVE = " SELECT " +
													 " e.id as idEpreuve," +
													 " e.dateDebutValidite," +
													 " e.dateFinValidite," +
													 " e.tempsEcoule," +
													 " e.etat," +
													 " e.note_obtenue" +
													 " e.niveau_obtenu," +
													 " e.idTest," +
													 " e.idUtilisateur," +
													 " qt.estMarquee," +
													 " qt.id as idQuestion," +
													 " qt.numordre," +
													 " qt.idEpreuve," +
													 " rt.id as idProposition," +
													 " rt.idQuestion," +
													 " rt.idEpreuve" +
													 " FROM" +
													 " EPREUVE E" +
													 " INNER JOIN QUESTION_TIRAGE qt ON e.id=qt.idEpreuve" +
													 " INNER JOIN REPONSE_TIRAGE rt ON qt.id=rt.idQuestion" +
													 " WHERE e.etat=?" +
													 " ORDER BY e.etat desc";
	
	public static final String SELECT_QUESTION_BY_IDTHEME_RANDOM = "SELECT TOP (?) * FROM QUESTION WHERE idTheme=? ORDER BY NEWID()";
	
	/**
	 * Méthode d'insertion d'épreuve, avec ses questions et les réponses liées aux questions
	 */
	@Override
	public void insertEpreuve(Epreuve epreuve) throws BusinessException {
		if (epreuve == null) {
			BusinessException businessExceptionEpreuve = new BusinessException();
			businessExceptionEpreuve.ajouterErreur(CodesResultatDAL.INSERT_EPREUVE_NULL);
			throw businessExceptionEpreuve;
		}
		
		try (Connection cnx = ConnectionProvider.getConnection()) {
			
			try {
				
				cnx.setAutoCommit(false);
				PreparedStatement pstmt = cnx.prepareStatement(INSERT_EPREUVE, PreparedStatement.RETURN_GENERATED_KEYS);
				pstmt.setDate(1, java.sql.Date.valueOf(epreuve.getDebutValidite()));
				pstmt.setDate(2, java.sql.Date.valueOf(epreuve.getFinValidite()));
				pstmt.setInt(3, epreuve.getTempsEcoule());
				pstmt.setInt(4, epreuve.getEtatEpreuve());
				pstmt.setFloat(5, epreuve.getNoteObtenue());
				pstmt.setString(6, epreuve.getNiveauObtenu());
				pstmt.setInt(7, epreuve.getIdTest());
				pstmt.setInt(8, epreuve.getIdUtilisateur());
				pstmt.executeUpdate();
				ResultSet rs = pstmt.getGeneratedKeys();
				if(rs.next()) {
					epreuve.setIdEpreuve(rs.getInt(1));
				}
				rs.close();
				pstmt.close();
				pstmt = cnx.prepareStatement(INSERT_QUESTION_TIRAGE, PreparedStatement.RETURN_GENERATED_KEYS);
				for(QuestionTirage questionT : epreuve.getListeQuestionT()) {
					pstmt.setInt(1, questionT.getMarquee());
					pstmt.setInt(2, questionT.getNumOrdre());
					pstmt.setInt(3, questionT.getIdEpreuve());
					pstmt.executeUpdate();
					rs = pstmt.getGeneratedKeys();
					if(rs.next()) {
						questionT.setIdEpreuve(epreuve.getIdEpreuve());
						questionT.setIdQuestion(rs.getInt(1));
					}
					rs.close();
				}
				pstmt.close();
				pstmt = cnx.prepareStatement(INSERT_REPONSE_TIRAGE, PreparedStatement.RETURN_GENERATED_KEYS);
				for(ReponseTirage reponseT : epreuve.getListeReponseT()) {
					pstmt.setInt(1, reponseT.getIdQuestion());
					pstmt.setInt(2, reponseT.getIdEpreuve());
					pstmt.executeUpdate();
					rs = pstmt.getGeneratedKeys();
					if(rs.next()) {
						reponseT.setIdEpreuve(epreuve.getIdEpreuve());
					}
					rs.close();
				}
				pstmt.close();
				cnx.commit();
				
			} catch (Exception e) {
				e.printStackTrace();
				cnx.rollback();
				throw e;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			BusinessException businessException = new BusinessException();
			businessException.ajouterErreur(CodesResultatDAL.INSERT_EPREUVE_ECHEC);
			throw businessException;
		}
	}

	/** 
	 * Méthode pour sélectionner une epreuve (Dans sa totalité), avec ses questions et les réponses liées aux questions
	 */
	@Override
	public List<Epreuve> selectEpreuve() throws BusinessException {
		List<Epreuve> listeEpreuve = new ArrayList<Epreuve>();
		
		try(Connection cnx = ConnectionProvider.getConnection()) {
			
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_ALL_EPREUVE);
			ResultSet rs = pstmt.executeQuery();
			Epreuve epreuveCourante=new Epreuve();
			
			while(rs.next()) {
				
				if(rs.getInt("idEpreuve")!=epreuveCourante.getIdEpreuve()) {
					
					epreuveCourante = epreuveBuilder(rs);
					listeEpreuve.add(epreuveCourante);
					
				}
				
				QuestionTirage questionTirage = questionTirageBuilder(rs);
				epreuveCourante.getListeQuestionT().add(questionTirage);
				
				ReponseTirage reponseTirage = reponseTirageBuilder(rs);
				QuestionTirage questionTCourante = new QuestionTirage();
				questionTCourante.getListeReponseT().add(reponseTirage);
			}
		} catch(Exception e) {
			e.printStackTrace();
			BusinessException businessException = new BusinessException();
			businessException.ajouterErreur(CodesResultatDAL.LECTURE_EPREUVE_ECHEC);
		}
		return listeEpreuve;
	}

	/** 
	 * Méthode pour sélectionner une epreuve en fonction de sa date de début, avec ses questions et les réponses liées aux questions
	 */
	@Override
	public List<Epreuve> selectEpreuveDebut(LocalDate Debut) throws BusinessException {
		List<Epreuve> listeEpreuve = new ArrayList<Epreuve>();
		try(Connection cnx = ConnectionProvider.getConnection()) {
			
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_DATE_DEBUT_EPREUVE);
			pstmt.setDate(1, java.sql.Date.valueOf(Debut));
			ResultSet rs = pstmt.executeQuery();
			Epreuve epreuveCourante = new Epreuve();
			
			while(rs.next()) {
				
				if(rs.getInt("idEpreuve")!=epreuveCourante.getIdEpreuve()) {
					
					epreuveCourante = epreuveBuilder(rs);
					listeEpreuve.add(epreuveCourante);
					
				}
				
				QuestionTirage questionTirage = questionTirageBuilder(rs);
				epreuveCourante.getListeQuestionT().add(questionTirage);
				
				ReponseTirage reponseTirage = reponseTirageBuilder(rs);
				QuestionTirage questionTCourante = new QuestionTirage();
				questionTCourante.getListeReponseT().add(reponseTirage);
			}
			
		} catch(Exception e) {
			e.printStackTrace();
			BusinessException businessException = new BusinessException();
			businessException.ajouterErreur(CodesResultatDAL.LECTURE_EPREUVE_ECHEC);
		}
		return listeEpreuve;
	}
	
	/** 
	 * Méthode pour sélectionner une epreuve en fonction de sa date de fin, avec ses questions et les réponses liées aux questions
	 */
	@Override
	public List<Epreuve> selectEpreuveFin(LocalDate Fin) throws BusinessException {
		List<Epreuve> listeEpreuve = new ArrayList<Epreuve>();
		try(Connection cnx = ConnectionProvider.getConnection()) {
			
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_DATE_FIN_EPREUVE);
			pstmt.setDate(1, java.sql.Date.valueOf(Fin));
			ResultSet rs = pstmt.executeQuery();
			Epreuve epreuveCourante = new Epreuve();
			
			while(rs.next()) {
				
				if(rs.getInt("idEpreuve")!=epreuveCourante.getIdEpreuve()) {
					
					epreuveCourante = epreuveBuilder(rs);
					listeEpreuve.add(epreuveCourante);
					
				}
				
				QuestionTirage questionTirage = questionTirageBuilder(rs);
				epreuveCourante.getListeQuestionT().add(questionTirage);
				
				ReponseTirage reponseTirage = reponseTirageBuilder(rs);
				QuestionTirage questionTCourante = new QuestionTirage();
				questionTCourante.getListeReponseT().add(reponseTirage);
			}
			
		} catch(Exception e) {
			e.printStackTrace();
			BusinessException businessException = new BusinessException();
			businessException.ajouterErreur(CodesResultatDAL.LECTURE_EPREUVE_ECHEC);
		}
		return listeEpreuve;
	}

	/* (non-Javadoc)
	 * @see fr.eni.javaee.qcm.dal.EpreuveDAO#selectEpreuve(int)
	 */
	@Override
	public List<Epreuve> selectEpreuve(int etatEpreuve) throws BusinessException {
		List<Epreuve> listeEpreuve = new ArrayList<Epreuve>();
		try(Connection cnx = ConnectionProvider.getConnection()) {
			Epreuve epreuveCourante = new Epreuve();
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_ETAT_EPREUVE);
			pstmt.setInt(1, epreuveCourante.getEtatEpreuve());
			ResultSet rs = pstmt.executeQuery();
			
			
			while(rs.next()) {
				
				if(rs.getInt("idEpreuve")!=epreuveCourante.getIdEpreuve()) {
					
					epreuveCourante = epreuveBuilder(rs);
					listeEpreuve.add(epreuveCourante);
					
				}
				
				QuestionTirage questionTirage = questionTirageBuilder(rs);
				epreuveCourante.getListeQuestionT().add(questionTirage);
				
				ReponseTirage reponseTirage = reponseTirageBuilder(rs);
				QuestionTirage questionTCourante = new QuestionTirage();
				questionTCourante.getListeReponseT().add(reponseTirage);
			}
			
		} catch(Exception e) {
			e.printStackTrace();
			BusinessException businessException = new BusinessException();
			businessException.ajouterErreur(CodesResultatDAL.LECTURE_EPREUVE_ECHEC);
		}
		return listeEpreuve;
	}
	

	

	private Epreuve epreuveBuilder(ResultSet rs) throws SQLException {
		Epreuve epreuveCourante;
		epreuveCourante=new Epreuve();
		epreuveCourante.setIdEpreuve(rs.getInt("idEpreuve"));
		epreuveCourante.setDebutValidite(rs.getDate("dateDebutValidite").toLocalDate());
		epreuveCourante.setFinValidite(rs.getDate("dateFinValidite").toLocalDate());
		epreuveCourante.setTempsEcoule(rs.getInt("tempsEcoule"));
		epreuveCourante.setEtatEpreuve(rs.getInt("etatEpreuve"));
		epreuveCourante.setNoteObtenue(rs.getFloat("noteObtenue"));
		epreuveCourante.setNiveauObtenu(rs.getString("niveauObtenu"));
		epreuveCourante.setIdTest(rs.getInt("idTest"));
		epreuveCourante.setIdUtilisateur(rs.getInt("idUtilisateur"));
		return epreuveCourante;
	}
	
	private QuestionTirage questionTirageBuilder(ResultSet rs) throws SQLException {
		QuestionTirage questionTirageCourante;
		questionTirageCourante=new QuestionTirage(rs.getBoolean("estMarquee"), rs.getInt("idQuestion"), rs.getInt("numordre"), rs.getInt("idEpreuve"));
		return questionTirageCourante;
		
	}
	
	private ReponseTirage reponseTirageBuilder(ResultSet rs) throws SQLException {
		ReponseTirage reponseTirageCourante;
		reponseTirageCourante=new ReponseTirage(rs.getInt("idProposition"),rs.getInt("idQuestion"), rs.getInt("idEpreuve"));
		return reponseTirageCourante;
	}

	
}
