package fr.eni.javaee.qcm.dal;

import fr.eni.javaee.qcm.bo.Utilisateur;

public interface UtilisateurDAO {

	public Utilisateur selectBy(String email, String password);
	
}
