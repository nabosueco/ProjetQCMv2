package fr.eni.javaee.qcm.dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import fr.eni.javaee.qcm.bo.Utilisateur;

/**
 * @author pierreCormier
 *
 */
public class UtilisateurDAOJdbcImpl implements UtilisateurDAO{

	private static final String SELECT="SELECT idUtilisateur, nom, prenom, email, password, codeProfil, codePromo FROM utilisateur WHERE email=? AND password=?;";
	
	@Override
	public Utilisateur selectBy(String email, String password) {
		Utilisateur user = null;
		
		try(Connection cnx = ConnectionProvider.getConnection())
		{
			PreparedStatement pstmt = cnx.prepareStatement(SELECT);
			pstmt.setString(1, email);
			pstmt.setString(2, password);
			
			ResultSet rs = pstmt.executeQuery();
			
			if (rs.next()) {
				user = new Utilisateur(
				rs.getInt("idUtilisateur"),
				rs.getString("nom"),
				rs.getString("prenom"),
				rs.getString("email"),
				rs.getString("password"),
				rs.getInt("codeProfil"),
				rs.getString("codePromo"));

				}
			} catch (SQLException e) {		
			e.printStackTrace();
		}
		
		return user;	
	}
}
