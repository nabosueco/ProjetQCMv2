package fr.eni.javaee.qcm.servlets;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.javaee.qcm.bll.PasswordTools;
import fr.eni.javaee.qcm.bll.UtilisateurManager;
import fr.eni.javaee.qcm.bo.Utilisateur;

/**
 * Servlet implementation class ServletAuthentification
 */
@WebServlet("/ServletAuthentification")
public class ServletAuthentification extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.invalidate();
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/authentification.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		// on récupère les données rentrées dans le formulaire d'authentification
		String nomUtilisateur = request.getParameter("utilisateur");
		String mdpUtilisateur = request.getParameter("motDePasse");
		
		// on instancie PasswordTools
		PasswordTools passwordTools = new PasswordTools();
		String mdpHash = null;
		
		try {
			mdpHash = passwordTools.hashMD5(mdpUtilisateur);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		System.out.println(mdpHash);
		
		// on instancie l'UtilisateurManager
		UtilisateurManager utilisateurManager = new UtilisateurManager();

		// on teste l'authentification de l'utilisateur (on vérifie qu'il est bien enregistré dans la base de données)
		Utilisateur utilisateur = utilisateurManager.authentifier(nomUtilisateur, mdpHash);
		
		// on enregistre et envoie en session les informations de l'utilisateur connecté et authentifié
		session.setAttribute("utilisateur", utilisateur);
		
		// on traduit le numero de codeProfil de l'utilisateur par une string de son rôle (candidat, formateur, responsable ou administrateur)
		if (utilisateur != null) {
			switch (utilisateur.getCodeProfil()) {
			case 0: 
					session.setAttribute("role", "Candidat");
					RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/candidatAccueil.jsp");
					rd.forward(request, response);
				break;
			case 1:
					session.setAttribute("role", "Formateur");
					RequestDispatcher rd1 = request.getRequestDispatcher("/WEB-INF/jsp/formateur.jsp");
					rd1.forward(request, response);
				break;
			case 2:
					session.setAttribute("role", "Responsable");
					RequestDispatcher rd2 = request.getRequestDispatcher("/WEB-INF/jsp/responsable.jsp");
					rd2.forward(request, response);
				break;
			case 3:
					session.setAttribute("role", "Administrateur");
					RequestDispatcher rd3 = request.getRequestDispatcher("/WEB-INF/jsp/administrateur.jsp");
					rd3.forward(request, response);
				break;
			}


		} else {
			String erreur = "erreur";
			request.setAttribute("erreur", erreur);
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/authentification.jsp");
			rd.forward(request, response);
		}
		
		
	}

}
