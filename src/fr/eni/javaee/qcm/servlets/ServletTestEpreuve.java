package fr.eni.javaee.qcm.servlets;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import fr.eni.javaee.qcm.bll.EpreuveManager;
import fr.eni.javaee.qcm.bo.Epreuve;
import fr.eni.javaee.qcm.bo.QuestionTirage;
import fr.eni.javaee.qcm.bo.ReponseTirage;

/**
 * Servlet implementation class ServletTestEpreuve
 */
@WebServlet("/ServletTestEpreuve")
public class ServletTestEpreuve extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//StringBuffer sgb = new StringBuffer();
		
		EpreuveManager em = new EpreuveManager();
		
		LocalDate test = LocalDate.ofYearDay(2018, 206);
		LocalDate test2 = LocalDate.ofYearDay(2018, 207);
		List<QuestionTirage> listeQT = new ArrayList<QuestionTirage>();
		List<ReponseTirage> listeRT = new ArrayList<ReponseTirage>();
		
		Epreuve e1 = new Epreuve(test,test2,2,0,20,"A/EC/N",1,1,listeQT,listeRT);
		try {
			em.ajouterEpreuve(e1);
			List<Epreuve> el = em.selectionnerToutesLesEpreuves();
			for(Epreuve e:el) {
				System.out.println(""+e.getIdEpreuve()+" "+e.getDebutValidite()+" "+e.getFinValidite()+" "+e.getTempsEcoule()+" "+e.getEtatEpreuve()+" "+e.getNoteObtenue()+" "+e.getNiveauObtenu()+" "+e.getIdTest()+" "+e.getIdUtilisateur());
			}
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	
		
	}

}
